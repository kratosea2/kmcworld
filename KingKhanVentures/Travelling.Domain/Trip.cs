﻿using System.Collections.Generic;

namespace KingKhanVentures.Travelling.Domain
{
    public class Trip
    {
        public int TravellingTime { get; set; }
        public List<Orbit> Orbits { get; set; }
        public Transport TransportType { get; set; }

        public Trip() { }

        public Trip(int travellingTime, Transport transportType, List<Orbit> orbits)
        {
            TravellingTime = TravellingTime;
            TransportType = transportType;
            Orbits = orbits;
        }

        public List<Orbit> GetOrbits()
        {
            return Orbits;
        }

        public Transport GetTransport()
        {
            return TransportType;
        }

        public string ShowTripInformation()
        {
            return $"Trip Details: [Travelling Time={TravellingTime}, Transportation={TransportType},"
                + $"Orbits= {Orbits}";
        }
    }
}
