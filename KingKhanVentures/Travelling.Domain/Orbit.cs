﻿namespace KingKhanVentures.Travelling.Domain
{
    public class Orbit
    {
        public string OrbitName { get; set; }
        public string SourceOrbit { get; set; }
        public string DestinationOrbit { get; set; }
        public double OrbitDistance { get; set; }
        public double RequiredCratersToCross { get; set; }
        public double SpeedLimit { get; set; }

        public Orbit() { }

        public Orbit(string orbitName, string sourceOrbit, string destinationOrbit, double orbitDistance, double requiredCratersToCross, double speedLimit)
        {
            OrbitName = orbitName;
            SourceOrbit = sourceOrbit;
            OrbitDistance = orbitDistance;
            DestinationOrbit = destinationOrbit;
            RequiredCratersToCross = requiredCratersToCross;
            SpeedLimit = speedLimit;
        }

        public string ShowSelectedOrbitToTravelTo()
        {
            return $"Orbit: [Source={SourceOrbit}, destination={DestinationOrbit},"
                + $"distance={OrbitDistance}, number of craters={RequiredCratersToCross},"
                + $"Speed Limit= {SpeedLimit}]";
        }
    }
}
