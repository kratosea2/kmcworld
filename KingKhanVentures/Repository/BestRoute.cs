﻿using System.Collections.Generic;
using KingKhanVentures.Travelling.Domain;

namespace KingKhanVentures.Repository
{
    public class BestRoute
    {
        private TravelLogic TripGuide = new TravelLogic();
        private InputValidator validator = new InputValidator();

        public string FindTheBestWayToTravel(string weatherOfTheDay, List<Orbit> orbits, Dictionary<string, double> orbitSpeedLimitMap)
        {
            var weatherTypeNotFoundMessage = validator.ValidateUserEnteredWeather(weatherOfTheDay);

            if (!string.IsNullOrEmpty(weatherTypeNotFoundMessage))
            {
                return weatherTypeNotFoundMessage;
            }

            Weather weather = TripGuide.GetWeather(weatherOfTheDay);
            List<Transport> transports = TripGuide.GetTransport(weather.TransportType);

            foreach (var orbit in orbits)
            {
                orbit.SpeedLimit = orbitSpeedLimitMap[orbit.OrbitName];
            }

            List<Trip> tripDetails = TripGuide.GetTripDetails(weather, transports, orbits);
            Trip tripDetail = TripGuide.DetermineBestTrip(tripDetails);

            if (tripDetail == null)
            {
                return "Unable to find the shortest route";
            }
            else
            {
                return TripGuide.DisplayTripDetails(tripDetail);
            }
        }

        public TravelLogic GetTravelInformation()
        {
            return TripGuide;
        }

    }
}
