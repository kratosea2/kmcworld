﻿using System;
using System.Collections.Generic;
using System.Linq;
using KingKhanVentures.Travelling.Domain;

namespace KingKhanVentures.UtilHelper
{
    public class TripInstantiator
    {

        public List<Weather> AssembleAllWeatherTypes()
        {
            List<Weather> weatherTypes = new List<Weather>();

            weatherTypes.Add(new Weather(WeatherType.weatherType.Sunny, -10,
                new List<string>() { "Bike", "Tuktuk", "Car" }));
            weatherTypes.Add(new Weather(WeatherType.weatherType.Rainy, 20,
                new List<string>() { "Tuktuk", "Car" }));
            weatherTypes.Add(new Weather(WeatherType.weatherType.Windy, 0,
               GetAllTransportTypes()));

            return weatherTypes;
        }

        public List<Transport> GetAllTransportModes()
        {
            List<Transport> transports = new List<Transport>();

            transports.Add(new Transport("Bike", 10, 2));
            transports.Add(new Transport("TukTuk", 12, 1));
            transports.Add(new Transport("Car", 20, 3));

            return transports;
        }

        public List<string> GetAllTransportTypes()
        {
            return GetAllTransportModes().Select(x => x.TransportType).ToList();
        }

        public List<string> GetTransportTypes()
        {
            return new List<string>() { "Bike", "Tuktuk", "Car" };
        }

        public List<Orbit> GetAvailableOrbits()
        {
            List<Orbit> orbits = new List<Orbit>();

            orbits.Add(new Orbit("Orbit 1", "Silk Drob", "Hallitharam", 18, 20, -1));
            orbits.Add(new Orbit("Orbit 2", "Silk Drob", "Hallitharam", 20, 10, -1));

            return orbits;
        }
    }
}
