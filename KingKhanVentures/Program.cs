﻿using KingKhanVentures.Repository;
using KingKhanVentures.Travelling.Domain;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace KingKhanVentures
{
    class Program
    {
        private static BestRoute RouteFinder = new BestRoute();

        static void Main(string[] args)
        {
            var weather = CaptureUserInputRelatedToWeather();

            var sourceOrbit = "Silk Drob";
            var destinationOrbit = "Hallitharam";

            List<Orbit> orbits = RouteFinder.GetTravelInformation().GetOrbits(sourceOrbit, destinationOrbit);
            Dictionary<string, double> orbitSpeedLimits = new Dictionary<string, double>();

            CapturingSpeedLimits(orbits, orbitSpeedLimits);

            Console.WriteLine("Expected Input: " + RouteFinder.FindTheBestWayToTravel(weather, orbits, orbitSpeedLimits));

            Console.ReadLine();
        }

        private static void CapturingSpeedLimits(List<Orbit> orbits, Dictionary<string, double> orbitSpeedLimits)
        {
            foreach (var orbit in orbits)
            {
                Console.WriteLine($"Enter speed limit for traffic in {orbit.OrbitName}");
                var orbitSpeedLimit = Console.ReadLine();

                Regex regularExpression = new Regex("^[a-zA-Z0-9]*$");

                while (string.IsNullOrEmpty(orbitSpeedLimit) || !regularExpression.IsMatch(orbitSpeedLimit))
                {
                    Console.WriteLine($"Only numbers can be entered \n please enter speed limit for traffic"
                        + $"in {orbit.OrbitName}");
                    orbitSpeedLimit = Console.ReadLine();
                }

                orbitSpeedLimits.Add(orbit.OrbitName, Double.Parse(orbitSpeedLimit));
            }
        }

        private static string CaptureUserInputRelatedToWeather()
        {
            Dictionary<int, string> weatherTypes = new Dictionary<int, string>();
            weatherTypes.Add(1, "Sunny");
            weatherTypes.Add(2, "Rainy");
            weatherTypes.Add(3, "Windy");

            Console.WriteLine($"Please enter the weather king Khan will travel in.\n"
                + $"1. For Sunny \n2. For Rainy \n3. For Windy");

            var weatherUserInput = Console.ReadLine();
            bool isValidEntry = IsWeatherValid(weatherUserInput);

            while (string.IsNullOrEmpty(weatherUserInput) || !isValidEntry)
            {
                Console.WriteLine($"Please make a valid selection  \n1.For Sunny \n2.For Rainy \n3.For Windy");
                weatherUserInput = Console.ReadLine();
                isValidEntry = IsWeatherValid(weatherUserInput);
            }

            return weatherTypes[int.Parse(weatherUserInput)];
        }

        private static bool IsWeatherValid(string weatherUserInput)
        {
            var regularExpression = new Regex("1|2|3");
            var isValidEntry = regularExpression.IsMatch(weatherUserInput);

            return isValidEntry;
        }
    }
}
