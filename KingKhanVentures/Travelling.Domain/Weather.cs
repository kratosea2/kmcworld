﻿using System;
using System.Collections.Generic;

namespace KingKhanVentures.Travelling.Domain
{
    public class Weather
    {
        public int CraterChangeRate { get; set; }
        public List<string> TransportType { get; set; }
        public WeatherType.weatherType weatherType { get; set; }

        public Weather(WeatherType.weatherType weatherType, int craterChangeRate, List<string> transportType)
        {
            this.weatherType = weatherType;
            CraterChangeRate = craterChangeRate;
            TransportType = transportType;
        }

        public Weather(string weatherType) { }

        public WeatherType.weatherType GetWeatherType()
        {
            return weatherType;
        }

        public String DisplayWeatherType()
        {
            return $"Weather: [Weather Type={weatherType}, Crater Change Rate={CraterChangeRate},"
                + "Transport Types={TransportType}";
        }
    }
}
