﻿
namespace KingKhanVentures.Travelling.Domain
{
    public class Transport
    {
        public double Speed { get; set; }
        public string TransportType { get; set; }
        public int TimeNeededToCrossCrater { get; set; }

        public Transport(string transportType, double speed, int timeNeededToCrossCrater)
        {
            Speed = speed;
            TransportType = transportType;
            TimeNeededToCrossCrater = timeNeededToCrossCrater;
        }

        public string SelectVehicleDisplay()
        {
            return $"Vehicle [{TransportType}, Speed={Speed},"
                + $"Time needed to cross Crater={TimeNeededToCrossCrater}";
        }


    }
}
