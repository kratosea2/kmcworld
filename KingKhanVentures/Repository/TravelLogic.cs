﻿using KingKhanVentures.Travelling.Domain;
using KingKhanVentures.UtilHelper;
using System.Collections.Generic;
using System;
using System.Linq;

namespace KingKhanVentures.Repository
{
    public class TravelLogic
    {
        private TripInstantiator instantiator = new TripInstantiator();

        public TravelLogic() { }

        public double GetOrbitSpeed(double orbitSpeedLimit)
        {
            return orbitSpeedLimit;
        }

        public Weather GetWeather(string weatherDuringTravel)
        {
            return instantiator.AssembleAllWeatherTypes().FirstOrDefault(x => weatherDuringTravel.Equals(x.GetWeatherType().ToString()
                , StringComparison.InvariantCultureIgnoreCase));
        }

        public List<Transport> GetTransport(List<string> transportType)
        {
            return instantiator.GetAllTransportModes().Where(x => transportType.Contains(x.TransportType))?.ToList();
        }

        public List<Trip> GetTripDetails(Weather weather, List<Transport> transportTypes, List<Orbit> orbits)
        {
            List<Trip> tripDetails = new List<Trip>();

            foreach (var transportType in transportTypes)
            {
                Trip tripDetail = new Trip();
                tripDetail.TravellingTime = CalculateTravellingTime(weather, transportType, orbits);
                tripDetail.Orbits = orbits;
                tripDetail.TransportType = transportType;

                tripDetails.Add(tripDetail);
            }

            return tripDetails;
        }

        public int CalculateTravellingTime(Weather weather, Transport transport, List<Orbit> orbits)
        {
            double craterToCrossOrbit = orbits.Sum(x => x.RequiredCratersToCross);
            double orbitDistance = orbits.Sum(x => x.OrbitDistance);
            double OrbitSpeedLimit = orbits.OrderBy(x => x.SpeedLimit).FirstOrDefault().SpeedLimit;
            double transportSpeed = transport.Speed;
            double orbitTraffixMaxSpeed = (OrbitSpeedLimit > transportSpeed) ? transportSpeed : OrbitSpeedLimit;
            double orbitNumberOfCrater = craterToCrossOrbit * ((100 + weather.CraterChangeRate) / 100.00);
            var travellingTime = ((orbitDistance * 60) / orbitTraffixMaxSpeed) + (orbitNumberOfCrater * transport.TimeNeededToCrossCrater);

            return (int)travellingTime;
        }

        public Trip DetermineBestTrip(List<Trip> travelDetails)
        {
            Trip bestTrip = null;

            var minimumTripDuration = Double.MaxValue;

            for (var i = 0; i < travelDetails.Count; i++)
            {
                var trip = travelDetails.ElementAt(i);

                if (trip.TravellingTime < minimumTripDuration)
                {
                    minimumTripDuration = trip.TravellingTime;
                    bestTrip = trip;
                }
                else if (trip.TravellingTime == minimumTripDuration)
                {
                    return trip;
                }
            }

            return bestTrip;
        }

        public List<Orbit> GetOrbits(string sourceOrbit, string destinationOrbit)
        {
            List<Orbit> orbits = new List<Orbit>();
            var existingObits = instantiator.GetAvailableOrbits().ToList();

            foreach (var existingOrbit in existingObits)
            {
                orbits.Add(existingOrbit);
            }

            return orbits;
        }

        public string DisplayTripDetails(Trip tripDetails)
        {
            var details = $"Vehicle {tripDetails.TransportType.TransportType} ";

            foreach (var orbitinfo in tripDetails.GetOrbits())
            {
                details += $" on {orbitinfo.OrbitName}";
            };

            return details;
        }
    }
}
