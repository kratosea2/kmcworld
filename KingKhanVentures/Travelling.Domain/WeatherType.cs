﻿using System;

namespace KingKhanVentures.Travelling.Domain
{
    public class WeatherType
    {
        public string Type { get; set; }

        public WeatherType() { }

        public bool isWeatherSelectionValid(string type)
        {
            if (Enum.IsDefined(typeof(weatherType), type))
            {
                Type = type;

                return true;
            }

            return false;
        }

        public enum weatherType
        {
            Sunny,
            Rainy,
            Windy
        }

        public string SelectWeatherType()
        {
            return Type;
        }
    }
}
