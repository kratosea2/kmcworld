﻿using KingKhanVentures.Travelling.Domain;

namespace KingKhanVentures.Repository
{
    public class InputValidator
    {
        public InputValidator() { }
        private static readonly log4net.ILog Logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string ValidateUserEnteredWeather(string weatherValue)
        {
            var weatherType = new WeatherType();

            if (!weatherType.isWeatherSelectionValid(weatherValue))
            {
                return $"Incorrect weather type entered";
            }

            return "";
        }
    }
}
