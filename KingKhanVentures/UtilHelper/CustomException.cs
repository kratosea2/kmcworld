﻿using System;

namespace KingKhanVentures.UtilHelper
{
    public class CustomException : Exception
    {
        public CustomException() { }

        public string ShowThrownException (Exception ex)
        {
            return ex.InnerException.Message;
        }
    }
}
